$(document).ready(function () {


	$('a[data-rel^=lightcase]').lightcase();

	if ($("#social_cunnect")[0]) {
		//For Social Data
		//-------------------------------
		var width = document.getElementById('social_cunnect').offsetWidth;
		var social_width = document.getElementsByClassName("social_cunnect");

		for (var i = 0; i < social_width.length; i++) {
			setHeight(social_width[i]);
		}

		function setHeight(div) {
			div.style.height = width + "px";
		}
		//	console.log(width);
	}
	//Init Slick on 'As seen in'
	//-----------------------------
	if ($(window).width() < 480) {
		$('.logos').slick({
			slidesToShow: 3,
			slidesToScroll: 3,
			autoplay: true,
			autoplaySpeed: 2000,
			arrows: false,
			centerMode: false,
			infinite: true,
			centerPadding: '40px',
			dots: true,
			responsive: [
				{
					breakpoint: 320,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	}

	//Init Slick on 'all_articles => article list'
	//-----------------------------
	if ($(window).width() < 769) {
		$('.all_articles').slick({
			slidesToShow: 2,
			slidesToScroll: 1,
			autoplay: false,
			autoplaySpeed: 2000,
			arrows: false,
			centerMode: true,
			centerPadding: '40px',
			dots: true,
			infinite: true,
			responsive: [
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	}

}); //-----------------------------------------
